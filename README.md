# Prevent SQL Injection vulnerabilities in PHP using PDO

WebApp Security exercises: Protect login feature from SQL injection vulnerabilities using PHP Data Objects (PDO)

- Không quen PHP mà Gv yêu cầu dùng PHP nên code hơi bị củ chuối :v

- Code không chăm chút nên còn nhiều lỗi, chỉ làm đúng yêu chống SQLi ở chức năng đăng nhập :v

- MD5 trong bài không mang ý nghĩa bảo vệ mật khẩu trong database [MD5](https://en.wikipedia.org/wiki/MD5) [Collision resistance](https://en.wikipedia.org/wiki/Collision_resistance)

- Có một số lỗ hổng trong bài được sinh ra để phá :v

***Cập nhật lần cuối: 2020***
